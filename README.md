# catkin workspace with packages manager

## Usage

First of all, you will need 'php' and some libraries: make sure to install
necessary packages:

    sudo apt-get install php php-xml

First, you'll need to install `catkin`, the most convenient method is the following

    sudo apt-get install python-pip python-empy
    sudo pip install -U catkin_tools


Then, clone this repository and enter it:

    git clone https://bitbucket.org/pfe_refactoring/workspace.git
    cd workspace

Run the setup:

    ./workspace setup

Install the model repository
    ./workspace install git@bitbucket.org:pfe_refactoring/model

## Commands

To pull all the repositories:

    ./workspace pull

To build:

    ./workspace build